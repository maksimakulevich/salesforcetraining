trigger OpportunityTrigger on Opportunity (before insert, before update, after update)
{

    TriggerTemplate.TriggerManager triggerManager = new TriggerTemplate.TriggerManager();
    triggerManager.addHandler(
            new OpportunityTriggerHandler(),
            new List<TriggerTemplate.TriggerAction>
            {
                TriggerTemplate.TriggerAction.beforeInsert,
                TriggerTemplate.TriggerAction.beforeUpdate,
                TriggerTemplate.TriggerAction.afterUpdate
            }
    );
    triggerManager.runHandlers();
}