trigger AccountTrigger on Account (before insert, before update)
{

    TriggerTemplate.TriggerManager triggerManager = new TriggerTemplate.TriggerManager();
    triggerManager.addHandler(
            new AccountTriggerHandler(),
            new List<TriggerTemplate.TriggerAction>
            {
                    TriggerTemplate.TriggerAction.beforeInsert,
                    TriggerTemplate.TriggerAction.beforeUpdate
            }
    );
    triggerManager.runHandlers();
}