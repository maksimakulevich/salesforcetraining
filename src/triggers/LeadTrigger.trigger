trigger LeadTrigger on Lead (before insert, before update, after insert, after update, after delete)
{

    TriggerTemplate.TriggerManager triggerManager = new TriggerTemplate.TriggerManager();
    triggerManager.addHandler(
            new LeadTriggerHandler(),
            new List<TriggerTemplate.TriggerAction>
            {
                    TriggerTemplate.TriggerAction.beforeInsert,
                    TriggerTemplate.TriggerAction.beforeUpdate,
                    TriggerTemplate.TriggerAction.afterInsert,
                    TriggerTemplate.TriggerAction.afterUpdate,
                    TriggerTemplate.TriggerAction.afterDelete
            }
    );
    triggerManager.runHandlers();
}