public with sharing class LeadService
{

    @TestVisible private static final String LEAD_SOURCE_WEB = 'Web';
    @TestVisible private static final String LEAD_SOURCE_CHANNEL_WEBSITE = 'Website';
    @TestVisible private static final String LEAD_SOURCE_PARTNER = 'Partner';

    public static void updateLeadStatus(List<Lead> newLeads, Map<Id, Lead> oldLeads)
    {
        for (Lead newLead : newLeads)
        {
            if ((newLead.LeadSource == LEAD_SOURCE_WEB || newLead.LeadSource_Channel__c == LEAD_SOURCE_CHANNEL_WEBSITE)
                && (oldLeads == null || (oldLeads != null && (newLead.LeadSource != oldLeads.get(newLead.Id).LeadSource
                || newLead.LeadSource_Channel__c != oldLeads.get(newLead.Id).LeadSource_Channel__c))))
            {
                newLead.Status = Label.Lead_Status_Working_Contacted;
            }
        }
    }

    public static void createPartnerAgreementRelatedToLead(List<Lead> newLeads, Map<Id, Lead> oldLeads)
    {
        List<Partner_Agreement__c> partnerAgreements = new List<Partner_Agreement__c>();
        for (Lead newLead : newLeads)
        {
            if (newLead.LeadSource == LEAD_SOURCE_PARTNER && (oldLeads == null || (oldLeads != null && (newLead.LeadSource
                != oldLeads.get(newLead.Id).LeadSource))))
            {
                partnerAgreements.add(new Partner_Agreement__c(Lead__c = newLead.Id));
            }
        }

        insert partnerAgreements;
    }

    public static void deletePartnerAgreementPreviouslyRelatedToLead(Map<Id, Lead> oldLeads)
    {
        List<Id> leadIDs = new List<Id>();
        for (Lead lead : oldLeads.values())
        {
            if (lead.LeadSource == LEAD_SOURCE_PARTNER)
            {
                leadIDs.add(lead.Id);
            }
        }

        if (leadIDs.isEmpty())
        {
            return;
        }

        List<Partner_Agreement__c> partnerAgreements = [
            SELECT Id
            FROM Partner_Agreement__c
            WHERE Lead__c IN :leadIDs];
        delete partnerAgreements;
    }

    public static void preventLeadConversionIfPartnerAgreementFieldIsBlank(List<Lead> newLeads, Map<Id, Lead> oldLeads)
    {
        List<Id> leadIDs = new List<Id>();
        for (Lead newLead : newLeads)
        {
            if (newLead.Status == Label.Lead_Status_Closed_Converted && (oldLeads == null || (oldLeads != null &&
                (newLead.Status != oldLeads.get(newLead.Id).Status))))
            {
                leadIDs.add(newLead.Id);
            }
        }

        if (leadIDs.isEmpty())
        {
            return;
        }

        List<Partner_Agreement__c> partnerAgreementsRelatedToLeads = [
            SELECT Terms_of_Cooperation__c, Lead__c
            FROM Partner_Agreement__c
            WHERE Lead__c IN :leadIDs];

        for (Partner_Agreement__c partnerAgreement : partnerAgreementsRelatedToLeads)
        {
            if (String.isBlank(partnerAgreement.Terms_of_Cooperation__c))
            {
                Trigger.newMap.get(partnerAgreement.Lead__c).addError(Label.Terms_of_Cooperation_Is_Blank_Error);
            }
        }
    }
}