@IsTest
private class LeadTriggerTest
{

    private static List<Lead> leads;

    static void dataSetup()
    {
        leads = new List<Lead>();
        for(Integer i = 0; i < 4; i++)
        {
            leads.add(new Lead(LastName = 'TestData', Company = 'TestCompany'));
        }
    }

    @IsTest
    static void updateLeadStatusTest()
    {
        dataSetup();

        leads[0].LeadSource = LeadService.LEAD_SOURCE_WEB;
        leads[1].LeadSource = LeadService.LEAD_SOURCE_WEB;
        leads[1].LeadSource_Channel__c = LeadService.LEAD_SOURCE_CHANNEL_WEBSITE;
        leads[2].LeadSource = 'Email';
        leads[3].LeadSource = 'Events';

        Test.startTest();
            insert leads;
        Test.stopTest();

        List<Lead> updatedLeads = [
            SELECT Status
            FROM Lead
            WHERE Id IN :leads];

        System.assertEquals(Label.Lead_Status_Working_Contacted, updatedLeads[0].Status);
        System.assertEquals(Label.Lead_Status_Working_Contacted, updatedLeads[1].Status);
        System.assertNotEquals(Label.Lead_Status_Working_Contacted, updatedLeads[2].Status);
        System.assertNotEquals(Label.Lead_Status_Working_Contacted, updatedLeads[3].Status);
    }

    @IsTest
    static void createPartnerAgreementRelatedToLeadTest()
    {
        dataSetup();

        leads[0].LeadSource = LeadService.LEAD_SOURCE_PARTNER;
        leads[0].No_Website__c = true;
        leads[1].LeadSource = LeadService.LEAD_SOURCE_PARTNER;
        leads[1].No_Website__c = true;
        leads[2].LeadSource = 'Email';
        leads[3].LeadSource = 'Events';

        Test.startTest();
            insert leads;
        Test.stopTest();

        List<Partner_Agreement__c> partnerAgreements = [
            SELECT Lead__c
            FROM Partner_Agreement__c
            WHERE Lead__c IN :leads];

        System.assert(partnerAgreements.size() == 2);
        System.assertEquals(leads[0].Id, partnerAgreements[0].Lead__c);
        System.assertEquals(leads[1].Id, partnerAgreements[1].Lead__c);
    }

    @IsTest
    static void deletePartnerAgreementPreviouslyRelatedToLeadTest()
    {
        dataSetup();

        leads[0].LeadSource = LeadService.LEAD_SOURCE_PARTNER;
        leads[0].No_Website__c = true;
        leads[1].LeadSource = LeadService.LEAD_SOURCE_PARTNER;
        leads[1].No_Website__c = true;
        leads[2].LeadSource = 'Email';
        leads[3].LeadSource = 'Events';

        Test.startTest();
            insert leads;
        Test.stopTest();

        List<Partner_Agreement__c> partnerAgreements = [
            SELECT Lead__c
            FROM Partner_Agreement__c
            WHERE Lead__c IN :leads];

        System.assert(partnerAgreements.size() == 2);
        System.assertEquals(leads[0].Id, partnerAgreements[0].Lead__c);
        System.assertEquals(leads[1].Id, partnerAgreements[1].Lead__c);

        leads[0].LeadSource = 'Email';
        leads[1].LeadSource = 'Events';

        update leads;

        partnerAgreements = [
            SELECT Lead__c
            FROM Partner_Agreement__c
            WHERE Lead__c IN :leads];

        System.assertEquals(true, partnerAgreements.isEmpty());
    }

    @IsTest
    static void deletePartnerAgreementPreviouslyRelatedToLeadWhenDeletedTest()
    {
        dataSetup();

        leads[0].LeadSource = LeadService.LEAD_SOURCE_PARTNER;
        leads[0].No_Website__c = true;
        leads[1].LeadSource = LeadService.LEAD_SOURCE_PARTNER;
        leads[1].No_Website__c = true;
        leads[2].LeadSource = 'Email';
        leads[3].LeadSource = 'Events';

        Test.startTest();
            insert leads;
        Test.stopTest();

        List<Partner_Agreement__c> partnerAgreements = [
            SELECT Lead__c
            FROM Partner_Agreement__c
            WHERE Lead__c IN :leads];

        System.assert(partnerAgreements.size() == 2);
        System.assertEquals(leads[0].Id, partnerAgreements[0].Lead__c);
        System.assertEquals(leads[1].Id, partnerAgreements[1].Lead__c);

        List<Id> leadIdsToCheckPartnerAgreement = new List<Id>
        {
            leads[0].Id,
            leads[1].Id
        };

        delete leads;

        partnerAgreements = [
            SELECT Lead__c
            FROM Partner_Agreement__c
            WHERE Lead__c IN :leadIdsToCheckPartnerAgreement];

        System.assertEquals(true, partnerAgreements.isEmpty());
    }

    @IsTest
    static void preventLeadConversionIfPartnerAgreementFieldIsBlankTest()
    {
        dataSetup();

        leads[0].LeadSource = LeadService.LEAD_SOURCE_PARTNER;
        leads[0].No_Website__c = true;
        leads[1].LeadSource = LeadService.LEAD_SOURCE_PARTNER;
        leads[1].No_Website__c = true;
        leads[2].LeadSource = LeadService.LEAD_SOURCE_WEB;
        leads[3].LeadSource = 'Events';

        Test.startTest();
            insert leads;
        Test.stopTest();

        leads[0].Status = Label.Lead_Status_Closed_Converted;
        leads[1].Status = Label.Lead_Status_Closed_Converted;

        String actualDmlException;

        try
        {
            update leads;
        }
        catch (DmlException dmlException)
        {
            actualDmlException = dmlException.getMessage();
        }

        System.assertEquals(true, actualDmlException.contains(Label.Terms_of_Cooperation_Is_Blank_Error));
    }
}