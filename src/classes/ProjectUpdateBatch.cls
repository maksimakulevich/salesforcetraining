public with sharing class ProjectUpdateBatch implements Database.Batchable<SObject>, Database.Stateful
{

    List<Database.SaveResult> invalidResults;
    List<Project__c> invalidProjects;
    @TestVisible private static final String STATUS_ACTIVE = 'Active';

    public Database.QueryLocator start(Database.BatchableContext batchableContext)
    {
        return Database.getQueryLocator([
            SELECT Start_Date__c, Status__c
            FROM Project__c
            WHERE Start_Date__c <= :Date.today()
                AND Status__c != :STATUS_ACTIVE]);
    }

    public void execute(Database.BatchableContext batchableContext, List<Project__c> projects)
    {
        for (Project__c project : projects)
        {
            project.Status__c = STATUS_ACTIVE;
        }

        Database.SaveResult[] updateResult = Database.update(projects, false);

        invalidResults = new List<Database.SaveResult>();
        invalidProjects = new List<Project__c>();

        for (Integer i = 0; i < updateResult.size(); i++)
        {
            if (!updateResult[i].isSuccess())
            {
                invalidResults.add(updateResult[i]);
                invalidProjects.add(projects[i]);
            }
        }
    }

    public void finish(Database.BatchableContext batchableContext)
    {
        if (!invalidResults.isEmpty())
        {
            MailSender.sendErrorReport(invalidResults, invalidProjects, 'ProjectUpdateBatch');
        }
    }
}