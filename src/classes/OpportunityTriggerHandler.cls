public with sharing class OpportunityTriggerHandler implements TriggerTemplate.Handler
{

	private List<Opportunity> newValues;
	private Map<Id, Opportunity> oldValues;

	public void setValues(List<SObject> newValues, Map<Id, SObject> oldValues)
	{
		this.newValues = newValues;
		this.oldValues = (Map<Id, Opportunity>) oldValues;
	}

	public void handle(TriggerTemplate.TriggerAction action)
	{
		if (action == TriggerTemplate.TriggerAction.beforeInsert)
		{
			OpportunityService.fillInCloseDateIfBlank(newValues);
			OpportunityService.setStageNameUponCreation(newValues);
		}
		if (action == TriggerTemplate.TriggerAction.beforeUpdate)
		{
			OpportunityService.blockUpdatingIfOpportunityIsClosed(newValues, oldValues);
			OpportunityService.fillInCloseDateWhenClosed(newValues, oldValues);
			OpportunityService.clearFieldsRelatedToClosedLostStatus(newValues, oldValues);
		}
		if (action == TriggerTemplate.TriggerAction.afterUpdate)
		{
			OpportunityService.sendEmailUponOpportunityClosure(newValues, oldValues);
		}
	}
}