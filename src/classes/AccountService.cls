public with sharing class AccountService
{

    public static void matchAccountBillingAddress(List<Account> newAccounts)
    {
        for (Account acct : newAccounts)
        {
            if (acct.Match_Billing_Address__c)
            {
                acct.ShippingPostalCode = acct.BillingPostalCode;
            }
        }
    }
}