@IsTest
public with sharing class ParkLocatorTest
{

    @IsTest
    static void parkLocatorTest()
    {
        Test.setMock(WebServiceMock.class, new ParkServiceMock());
        List<String> result = ParkLocator.country('Grand Canyon');
        String actualResult = String.join(result, ',');
        String expectedResult = 'USA';
        System.assertEquals(expectedResult, actualResult);
    }
}