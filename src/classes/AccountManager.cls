@RestResource(UrlMapping='/Accounts/*/contacts')
global with sharing class AccountManager
{

    @HttpGet
    global static Account getAccount()
    {
        RestRequest request = RestContext.request;
        String accountId = request.requestURI.substringBetween('Accounts/', '/contacts');
        List<Account> desiredAccount = [
                SELECT Id, Name, (SELECT Id, Name FROM Contacts)
                FROM Account
                WHERE Id = :accountId
                LIMIT 1
        ];

        return desiredAccount[0];
    }
}