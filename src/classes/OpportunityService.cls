public with sharing class OpportunityService
{

    public static void fillInCloseDateIfBlank(List<Opportunity> newOpportunities)
    {
        for (Opportunity newOpportunity : newOpportunities)
        {
            if (newOpportunity.CloseDate == null)
            {
				newOpportunity.CloseDate = Date.today().addMonths(3);
            }
        }
    }

    public static void fillInCloseDateWhenClosed(List<Opportunity> newOpportunities, Map<Id, Opportunity> oldOpportunities)
    {
        for (Opportunity newOpportunity : newOpportunities)
		{
			if ((newOpportunity.StageName == Label.Opportunity_Stage_Closed_Won
				|| newOpportunity.StageName == Label.Opportunity_Stage_Closed_Lost && newOpportunity.StageName
				!= oldOpportunities.get(newOpportunity.Id).StageName))
			{
				newOpportunity.CloseDate = Date.today();
			}
		}
    }

	public static void blockUpdatingIfOpportunityIsClosed(List<Opportunity> newOpportunities, Map<Id, Opportunity> oldOpportunities)
	{
		List<String> opportunityFieldsEligibleForUpdate = Label.Opportunity_Fields_Eligible_For_Update.trim().split(';');
		List<String> apiFieldNames = new List<String>();
		for (Schema.SObjectField field : Opportunity.SObjectType.getDescribe().fields.getMap().values())
		{
			apiFieldNames.add(field.getDescribe().getName());
		}

		for (Opportunity newOpportunity : newOpportunities)
		{
			if (newOpportunity.StageName == Label.Opportunity_Stage_Closed_Won
				&& newOpportunity.StageName == oldOpportunities.get(newOpportunity.Id).StageName)
			{
				Trigger.newMap.get(newOpportunity.Id).addError(Label.Cannot_Update_Closed_Won_Opportunities);
			}
			else if (newOpportunity.StageName == Label.Opportunity_Stage_Closed_Lost
				&& newOpportunity.StageName == oldOpportunities.get(newOpportunity.Id).StageName)
			{
				for (Integer i = 0; i < apiFieldNames.size(); i++)
				{
					if (newOpportunity.get(apiFieldNames[i]) != oldOpportunities.get(newOpportunity.Id).get(apiFieldNames[i])
						&& !opportunityFieldsEligibleForUpdate.contains(apiFieldNames[i]))
					{
						Trigger.newMap.get(newOpportunity.Id).addError(Label.Cannot_Update_Closed_Lost_Opportunities);
					}
				}
			}
		}
	}

	public static void clearFieldsRelatedToClosedLostStatus(List<Opportunity> newOpportunities, Map<Id, Opportunity> oldOpportunities)
	{
		for (Opportunity newOpportunity : newOpportunities)
		{
			if (oldOpportunities.get(newOpportunity.Id).StageName == Label.Opportunity_Stage_Closed_Lost
				&& (newOpportunity.StageName != oldOpportunities.get(newOpportunity.Id).StageName))
			{
				newOpportunity.Closed_Lost_Reason__c = '';
				newOpportunity.Closed_Lost_Comments__c = '';
			}
		}
	}

	public static void setStageNameUponCreation(List<Opportunity> newOpportunities)
	{
		for (Opportunity newOpportunity : newOpportunities)
		{
			newOpportunity.StageName = Label.Opportunity_Stage_Prospecting;
		}
	}

	public static void sendEmailUponOpportunityClosure(List<Opportunity> newOpportunities, Map<Id, Opportunity> oldOpportunities)
	{
		List<Opportunity> closedOpportunities = new List<Opportunity>();
		for (Opportunity newOpportunity : newOpportunities)
		{
			if ((newOpportunity.StageName == Label.Opportunity_Stage_Closed_Won
				|| newOpportunity.StageName == Label.Opportunity_Stage_Closed_Lost && newOpportunity.StageName
				!= oldOpportunities.get(newOpportunity.Id).StageName))
			{
				closedOpportunities.add(newOpportunity);
			}
		}

		if (closedOpportunities.isEmpty())
		{
			return;
		}

		List<Lead> leadsConvertedToOpportunities = [
			SELECT ConvertedOpportunityId
			FROM Lead
			WHERE ConvertedOpportunityId IN :closedOpportunities];

		Set<Id> convertedOpportunitiesIds = new Set<Id>();
		for (Lead lead : leadsConvertedToOpportunities)
		{
			convertedOpportunitiesIds.add(lead.ConvertedOpportunityId);
		}

		Email_Info__mdt convertedLostOpportunityNotification = Email_Info__mdt.getInstance(Label.Converted_Lost_Opportunity_Notification);
		Email_Info__mdt convertedWonOpportunityNotification = Email_Info__mdt.getInstance(Label.Converted_Won_Opportunity_Notification);
		Email_Info__mdt standaloneLostOpportunityNotification = Email_Info__mdt.getInstance(Label.Standalone_Lost_Opportunity_Notification);
		Email_Info__mdt standaloneWonOpportunityNotification = Email_Info__mdt.getInstance(Label.Standalone_Won_Opportunity_Notification);

		if (convertedLostOpportunityNotification == null || convertedWonOpportunityNotification == null
			|| standaloneLostOpportunityNotification == null || standaloneWonOpportunityNotification == null)
		{
			return;
		}

		List<EmailTemplate> emailTemplates = [
			SELECT Id, DeveloperName
			FROM EmailTemplate
			WHERE DeveloperName = :convertedLostOpportunityNotification.Email_Template_Name__c
				OR DeveloperName = :convertedWonOpportunityNotification.Email_Template_Name__c];

		if (emailTemplates.isEmpty())
		{
			return;
		}

		Map<String, Id> emailTemplatesIdsByStrings = new Map<String, Id>();
		for (EmailTemplate emailTemplate : emailTemplates)
		{
			emailTemplatesIdsByStrings.put(emailTemplate.DeveloperName, emailTemplate.Id);
		}

		List<Messaging.SingleEmailMessage> emailMessages = new List<Messaging.SingleEmailMessage>();
		for (Opportunity closedOpportunity : closedOpportunities)
		{
			if (closedOpportunity.StageName == Label.Opportunity_Stage_Closed_Won)
			{
				if (convertedOpportunitiesIds.contains(closedOpportunity.Id))
				{
					emailMessages.add(MailSender.generateEmailMessage(closedOpportunity.Id,
						emailTemplatesIdsByStrings.get(convertedWonOpportunityNotification.Email_Template_Name__c),
						convertedWonOpportunityNotification));
				}
				else
				{
					emailMessages.add(MailSender.generateEmailMessage(closedOpportunity.Id,
						emailTemplatesIdsByStrings.get(standaloneWonOpportunityNotification.Email_Template_Name__c),
						standaloneWonOpportunityNotification));
				}
			}
			else if (closedOpportunity.StageName == Label.Opportunity_Stage_Closed_Lost)
			{
				if (convertedOpportunitiesIds.contains(closedOpportunity.Id))
				{
					emailMessages.add(MailSender.generateEmailMessage(closedOpportunity.Id,
						emailTemplatesIdsByStrings.get(convertedLostOpportunityNotification.Email_Template_Name__c),
						convertedLostOpportunityNotification));
				}
				else
				{
					emailMessages.add(MailSender.generateEmailMessage(closedOpportunity.Id,
						emailTemplatesIdsByStrings.get(standaloneLostOpportunityNotification.Email_Template_Name__c),
						standaloneLostOpportunityNotification));
				}
			}
		}

		MailSender.sendEmail(emailMessages);
	}
}