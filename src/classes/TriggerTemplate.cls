global without sharing class TriggerTemplate
{

    public enum TriggerAction
    {
        afterDelete, afterInsert, afterUndelete,
        afterUpdate, beforeDelete, beforeInsert, beforeUpdate
    }

    public interface Handler
    {
        void handle(TriggerTemplate.TriggerAction theAction);
        void setValues(List<SObject> theNewValues, Map<Id, SObject> theOldValues);
    }

    public class TriggerManager
    {

        private Map<String, List<Handler>> eventHandlerMapping = new Map<String, List<Handler>>();

        public void addHandler(Handler theHandler, TriggerTemplate.TriggerAction theAction)
        {
            List<Handler> aHandlers = eventHandlerMapping.get(theAction.name());
            if (aHandlers == null)
            {
                aHandlers = new List<Handler>();
                eventHandlerMapping.put(theAction.name(), aHandlers);
            }
            aHandlers.add(theHandler);
        }

        public void addHandler(Handler theHandler, List<TriggerTemplate.TriggerAction> theActions)
        {
            for (TriggerAction anAction: theActions)
            {
                addHandler(theHandler, anAction);
            }
        }

        public void runHandlers()
        {
            TriggerAction theAction = null;
            if (Trigger.isBefore)
            {
                if (Trigger.isInsert)
                {
                    theAction = TriggerTemplate.TriggerAction.beforeInsert;
                } else if (Trigger.isUpdate)
                {
                    theAction = TriggerTemplate.TriggerAction.beforeUpdate;
                } else if (Trigger.isDelete)
                {
                    theAction = TriggerTemplate.TriggerAction.beforeDelete;
                }
            }
            else if (Trigger.isAfter)
            {
                if (Trigger.isInsert)
                {
                    theAction = TriggerTemplate.TriggerAction.afterInsert;
                } else if (Trigger.isUpdate)
                {
                    theAction = TriggerTemplate.TriggerAction.afterUpdate;
                } else if (Trigger.isDelete)
                {
                    theAction = TriggerTemplate.TriggerAction.afterDelete;
                } else if (Trigger.isUndelete)
                {
                    theAction = TriggerTemplate.TriggerAction.afterUndelete;
                }
            }

            List<Handler> aHandlers = eventHandlerMapping.get(theAction.name());
            if (aHandlers != null && !aHandlers.isEmpty())
            {
                for (Handler aHandler : aHandlers)
                {
                    aHandler.setValues(Trigger.new, Trigger.oldMap);
                    aHandler.handle(theAction);
                }
            }
        }
    }
}