@IsTest
public with sharing class RateUpdateWsTest
{

	private static final Decimal EUR_EXCHANGE_RATE = 0.885638;
	private static final Decimal GBP_EXCHANGE_RATE = 0.740612;
	private static Map<String, Object> RESULTS;
	private static Map<String, Object> RESULTS_FOR_NON_EXISTENT_RATES;
	private static String EXPECTED_POST_SUB_REQUEST;
	private static String EXPECTED_PATCH_SUB_REQUEST;
	private static String START_DATE;
	private static DatedConversionRate NEW_DATED_CONVERSION_RATE;
	private static DatedConversionRate EXISTING_DATED_CONVERSION_RATE;
	private static Datetime CONVERSION_RATE_DATE;
	private static String EXPECTED_REQUEST_BODY;
	private static String EXPECTED_REQUEST_BODY_FOR_NEW_RECORDS;
	private static Integer SUCCESS_CODE;
	private static Integer ERROR_CODE;
	private static String SUCCESS_RESPONSE;
	private static String ERROR_RESPONSE;

	static void dataSetup()
	{
		START_DATE = Datetime.newInstance(2022, 1, 27).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
		RESULTS = (Map<String, Object>) JSON.deserializeUntyped(
			'{ "hasErrors" : false, "timestamp": 1643187600, "rates" : { "EUR" : 0.885638, ' +
				'"GBP" : 0.732881, "BYN" : 2.59825, "AUD" : 1.401296}}');
		RESULTS_FOR_NON_EXISTENT_RATES = (Map<String, Object>) JSON.deserializeUntyped('{ "hasErrors" : ' +
			'false, "timestamp": 1643702400, "rates" : { "EUR" : 0.885638, ' +
			'"GBP" : 0.732881, "BYN" : 2.59825, "AUD" : 1.401296}}');
		EXPECTED_REQUEST_BODY = '{ "batchRequests" : [{ "method" : "PATCH", "url" : ' +
			'"v53.0/sobjects/DatedConversionRate/04w5j000000CcasAAC", "richInput" : { ' +
			'"IsoCode" : "EUR", "ConversionRate" : 0.885638, "StartDate" : "2022-01-26T01:00:00Z" }},' +
			'{ "method" : "PATCH", "url" : "v53.0/sobjects/DatedConversionRate/04w5j000000CcaoAAC", ' +
			'"richInput" : { "IsoCode" : "GBP", "ConversionRate" : 0.732881, "StartDate" : "2022-01-26T01:00:00Z" }},' +
			'{ "method" : "PATCH", "url" : "v53.0/sobjects/DatedConversionRate/04w5j000000CcanAAC", "richInput" : ' +
			'{ "IsoCode" : "BYN", "ConversionRate" : 2.59825, "StartDate" : "2022-01-26T01:00:00Z" }},{ "method" : ' +
			'"POST", "url" : "v53.0/sobjects/DatedConversionRate", "richInput" : { "IsoCode" : "AUD", "ConversionRate" : ' +
			'1.401296, "StartDate" : "2022-01-26T01:00:00Z" }}]}';
		EXPECTED_REQUEST_BODY_FOR_NEW_RECORDS = '{ "batchRequests" : [{ "method" : "POST", "url" : ' +
			'"v53.0/sobjects/DatedConversionRate", "richInput" : { ' +
			'"IsoCode" : "EUR", "ConversionRate" : 0.885638, "StartDate" : "2022-02-01T00:00:00Z" }},' +
			'{ "method" : "POST", "url" : "v53.0/sobjects/DatedConversionRate", ' +
			'"richInput" : { "IsoCode" : "GBP", "ConversionRate" : 0.732881, "StartDate" : "2022-02-01T00:00:00Z" }},' +
			'{ "method" : "POST", "url" : "v53.0/sobjects/DatedConversionRate", "richInput" : ' +
			'{ "IsoCode" : "BYN", "ConversionRate" : 2.59825, "StartDate" : "2022-02-01T00:00:00Z" }},{ "method" : ' +
			'"POST", "url" : "v53.0/sobjects/DatedConversionRate", "richInput" : { "IsoCode" : "AUD", "ConversionRate" : ' +
			'1.401296, "StartDate" : "2022-02-01T00:00:00Z" }}]}';
		NEW_DATED_CONVERSION_RATE = new DatedConversionRate(IsoCode = 'GBP');
		EXISTING_DATED_CONVERSION_RATE = [
			SELECT IsoCode
			FROM DatedConversionRate
			WHERE StartDate = 2022-01-26
				AND IsoCode = 'EUR'
			LIMIT 1];
		EXPECTED_POST_SUB_REQUEST = '{ \"method\" : \"POST\", \"url\" : ' +
			'\"v53.0/sobjects/DatedConversionRate\", \"richInput\" : '
			+ '{ \"IsoCode\" : \"GBP\", \"ConversionRate\" : ' + GBP_EXCHANGE_RATE
			+ ', \"StartDate\" : \"' + START_DATE + '\" }}';
		EXPECTED_PATCH_SUB_REQUEST = '{ \"method\" : \"PATCH\", \"url\" : ' +
			'\"v53.0/sobjects/DatedConversionRate/' + EXISTING_DATED_CONVERSION_RATE.Id + '\", \"richInput\" : '
			+ '{ \"IsoCode\" : \"EUR\", \"ConversionRate\" : ' + EUR_EXCHANGE_RATE
			+ ', \"StartDate\" : \"' + START_DATE + '\" }}';
		CONVERSION_RATE_DATE = Datetime.newInstance(2022, 1, 27);
		SUCCESS_CODE = 200;
		ERROR_CODE = 400;
		SUCCESS_RESPONSE = '{ "hasErrors" : true, "results" : { "statusCode" : 201, "result" : [' +
			'{ "id" : "04w5j000000CcanAAC", "errors" : "Invalid field value" }, { "id" : "04w5j000000CcasAAC", ' +
			'"errors" : "Invalid field value" } ]}}';
		ERROR_RESPONSE = '{ "hasErrors" : true, "results" : [{ "result" : [{ "errorCode" : "NOT_FOUND", "message" : "The requested resource does not exist"}] }]}';
	}

	@IsTest
	static void createSubRequest()
	{
		dataSetup();
		String actualPostSubRequest = RateUpdateWs.createSubRequest(NEW_DATED_CONVERSION_RATE,
			GBP_EXCHANGE_RATE, CONVERSION_RATE_DATE);
		String actualPatchSubRequest = RateUpdateWs.createSubRequest(EXISTING_DATED_CONVERSION_RATE,
			EUR_EXCHANGE_RATE, CONVERSION_RATE_DATE);

		System.assertEquals(EXPECTED_POST_SUB_REQUEST, actualPostSubRequest);
		System.assertEquals(EXPECTED_PATCH_SUB_REQUEST, actualPatchSubRequest);
	}

	@IsTest
	static void processResponseTest()
	{
		dataSetup();
		String actualRequestBody = RateUpdateWs.processResponse(RESULTS);

		System.assertEquals(EXPECTED_REQUEST_BODY, actualRequestBody);

		String actualRequestBodyForNewRecords = RateUpdateWs.processResponse(RESULTS_FOR_NON_EXISTENT_RATES);
		System.assertEquals(EXPECTED_REQUEST_BODY_FOR_NEW_RECORDS, actualRequestBodyForNewRecords);
	}

	@IsTest
	static void updateDatedConversionRatesSuccessCodeTest()
	{
		dataSetup();

		Test.setMock(HttpCalloutMock.class, new RateUpdateWsMock(SUCCESS_CODE, SUCCESS_RESPONSE));

		RateUpdateWs.updateDatedConversionRates(EXPECTED_REQUEST_BODY);
	}

	@IsTest
	static void updateDateConversionRatesErrorCodeTest()
	{
		dataSetup();

		Test.setMock(HttpCalloutMock.class, new RateUpdateWsMock(ERROR_CODE, ERROR_RESPONSE));

		RateUpdateWs.updateDatedConversionRates(EXPECTED_REQUEST_BODY);
	}

	@IsTest
	static void executeTest()
	{
		System.schedule('RateUpdateWs', '0 0 */6 * * ?', new RateUpdateWs());

		List<CronTrigger> jobs = [
			SELECT Id, CronJobDetail.Name, State
			FROM CronTrigger
			WHERE CronJobDetail.Name = 'RateUpdateWs'];

		System.assertEquals('RateUpdateWs', jobs[0].CronJobDetail.Name);
	}
}