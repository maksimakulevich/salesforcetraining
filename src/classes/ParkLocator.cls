public with sharing class ParkLocator
{

    public static String[] country(String parkName)
    {
        ParkService.ParksImplPort park = new ParkService.ParksImplPort();
        return park.byCountry(parkName);
    }
}