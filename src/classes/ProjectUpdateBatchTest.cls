@IsTest
public with sharing class ProjectUpdateBatchTest
{

	private static List<Project__c> projects;

	static void dataSetup()
	{
		projects = new List<Project__c>
		{
			new Project__c(Start_Date__c = Date.today().addDays(-1), Status__c = 'Planned'),
			new Project__c(Start_Date__c = Date.today().addDays(-2), Status__c = 'Planned'),
			new Project__c(Start_Date__c = Date.today().addDays(-3), Status__c = 'Planned'),
			new Project__c(Start_Date__c = Date.today().addDays(-4), Status__c = 'Planned'),
			new Project__c(Start_Date__c = Date.today().addDays(-5), Status__c = 'Planned'),
			new Project__c(Start_Date__c = Date.today().addDays(-6), Status__c = ProjectUpdateBatch.STATUS_ACTIVE),
			new Project__c(Start_Date__c = Date.today().addDays(-7), Status__c = ProjectUpdateBatch.STATUS_ACTIVE),
			new Project__c(Start_Date__c = Date.today().addDays(-8), Status__c = ProjectUpdateBatch.STATUS_ACTIVE),
			new Project__c(Start_Date__c = Date.today().addDays(-9), Status__c = ProjectUpdateBatch.STATUS_ACTIVE),
			new Project__c(Start_Date__c = Date.today().addDays(-10), Status__c = ProjectUpdateBatch.STATUS_ACTIVE),
			new Project__c(Start_Date__c = Date.today().addDays(1), Status__c = 'Inactive'),
			new Project__c(Start_Date__c = Date.today().addDays(2), Status__c = 'Inactive'),
			new Project__c(Start_Date__c = Date.today().addDays(3), Status__c = 'Inactive'),
			new Project__c(Start_Date__c = Date.today().addDays(4), Status__c = 'Inactive'),
			new Project__c(Start_Date__c = Date.today().addDays(5), Status__c = 'Inactive'),
			new Project__c(Start_Date__c = Date.today().addDays(6), Status__c = ProjectUpdateBatch.STATUS_ACTIVE),
			new Project__c(Start_Date__c = Date.today().addDays(7), Status__c = ProjectUpdateBatch.STATUS_ACTIVE),
			new Project__c(Start_Date__c = Date.today().addDays(8), Status__c = ProjectUpdateBatch.STATUS_ACTIVE),
			new Project__c(Start_Date__c = Date.today().addDays(9), Status__c = ProjectUpdateBatch.STATUS_ACTIVE),
			new Project__c(Start_Date__c = Date.today().addDays(10), Status__c = ProjectUpdateBatch.STATUS_ACTIVE)
		};
	}

	@IsTest
	static void projectUpdateBatchTest()
	{
		dataSetup();

		insert projects;

		Test.startTest();
            ProjectUpdateBatch projectUpdateBatchInstance = new ProjectUpdateBatch();
            Database.executeBatch(projectUpdateBatchInstance);
		Test.stopTest();

		List<Project__c> updatedProjects = [
			SELECT Status__c
			FROM Project__c];

		System.assertEquals(ProjectUpdateBatch.STATUS_ACTIVE, updatedProjects[0].Status__c);
		System.assertEquals(ProjectUpdateBatch.STATUS_ACTIVE, updatedProjects[1].Status__c);
		System.assertEquals(ProjectUpdateBatch.STATUS_ACTIVE, updatedProjects[2].Status__c);
		System.assertEquals(ProjectUpdateBatch.STATUS_ACTIVE, updatedProjects[3].Status__c);
		System.assertEquals(ProjectUpdateBatch.STATUS_ACTIVE, updatedProjects[4].Status__c);
        System.assertNotEquals(ProjectUpdateBatch.STATUS_ACTIVE, updatedProjects[10].Status__c);
        System.assertNotEquals(ProjectUpdateBatch.STATUS_ACTIVE, updatedProjects[11].Status__c);
        System.assertNotEquals(ProjectUpdateBatch.STATUS_ACTIVE, updatedProjects[12].Status__c);
	}
}