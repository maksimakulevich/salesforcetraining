public class LeadProcessor implements Database.Batchable<SObject>
{
    public Database.QueryLocator start(Database.BatchableContext batchableContext)
    {
        return Database.getQueryLocator([SELECT LeadSource FROM Lead]);
    }

    public void execute(Database.BatchableContext batchableContext, List<Lead> leads)
    {
        for (Lead lead : leads)
        {
            lead.LeadSource = 'DreamForce';
        }
        update leads;
    }

    public void finish(Database.BatchableContext batchableContext)
    {

    }
}