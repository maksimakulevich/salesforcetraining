public class AccountProcessor
{
    @Future
    public static void countContacts(List<Id> accountIds)
    {
        List<Account> accounts = [SELECT Id, Name FROM Account WHERE Id IN :accountIds];
        for (Account acc : accounts)
        {
            acc.Number_of_Contacts__c = [SELECT COUNT() FROM Contact WHERE Contact.AccountId = :acc.Id];
            update acc;
        }
    }
}