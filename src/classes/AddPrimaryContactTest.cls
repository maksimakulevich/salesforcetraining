@IsTest
public class AddPrimaryContactTest
{
    @IsTest
    static void TestAddPrimaryContactTest()
    {
        List<Account> testList = new List<Account>();

        for (Integer i = 0; i < 50; i++)
        {
            testList.add(new Account(BillingState = 'NY', Name = 'Test' + i));
            testList.add(new Account(BillingState = 'CA', Name = 'Test' + i));
        }
        insert testList;

        AddPrimaryContact addPrimaryContact = new AddPrimaryContact(new Contact(FirstName = 'FN', LastName = 'LN'), 'CA');

        Test.startTest();
        System.enqueueJob(addPrimaryContact);
        Test.stopTest();
    }
}