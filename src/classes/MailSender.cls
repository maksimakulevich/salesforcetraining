public with sharing class MailSender
{

    public static void sendErrorReport(List<Database.SaveResult> saveResults, List<SObject> records, String errorTrace)
    {
        Email_Info__mdt emailInfo = Email_Info__mdt.getInstance('Akulevich_Project_Support');

        if (emailInfo == null)
        {
            return;
        }

        String errorLog = 'Unable to update records.\n\n'
            + 'The issue occurred in ' + errorTrace + '.\n\n'
            + 'The following records caused an exception:\n';
        for (Integer i = 0; i < saveResults.size(); i++)
        {
            errorLog += records[i].Id + ' ';
            errorLog += '(' + saveResults[i].getErrors() + ')';
        }

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(emailInfo.Recipients__c.trim().split(';'));
        mail.setSenderDisplayName(emailInfo.SenderDisplayName__c);
        mail.setSubject(emailInfo.Subject__c);
        mail.setPlainTextBody(errorLog);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

    public static void sendErrorReport(List<Object> errorMessages, String statusCode, String errorTrace)
    {
        Email_Info__mdt emailInfo = Email_Info__mdt.getInstance('Akulevich_Project_Support');

        if (emailInfo == null)
        {
            return;
        }

        String errorLog = 'Unable to process records.\n\n'
            + 'The issue occurred in ' + errorTrace + ' due to status code ' + statusCode + '.\n\n';

        List<String> errorCodes = new List<String>();
        List<String> messages = new List<String>();
        for (Object errorMessage : errorMessages)
        {
            List<String> splitErrorMessage = errorMessage.toString().split(',');
            errorCodes.add(splitErrorMessage[0].replace('{result=({errorCode=', ''));
            messages.add(splitErrorMessage[1].replace(' message=', '').replace('}', ''));
        }

        for (Integer i = 0; i < errorCodes.size(); i++)
        {
            errorLog += errorCodes[i] + ' ';
            errorLog += '(' + messages[i] + ')\n';
        }

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new List<String> { emailInfo.Email__c });
        mail.setSenderDisplayName(emailInfo.SenderDisplayName__c);
        mail.setSubject(emailInfo.Subject__c);
        mail.setPlainTextBody(errorLog);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

    public static void sendErrorReport(List<Object> recordsWithErrors, String errorTrace)
    {
        Email_Info__mdt emailInfo = Email_Info__mdt.getInstance('Akulevich_Project_Support');

        if (emailInfo == null)
        {
            return;
        }

        String errorLog = 'Unable to process records.\n\n'
            + 'The issue occurred in ' + errorTrace + '.\n\n'
            + 'The following records caused an exception:\n';

        List<String> recordsIds = new List<String>();
        List<String> errors = new List<String>();
        for (Object errorInfo : recordsWithErrors)
        {
            List<String> splitErrorInfo = errorInfo.toString().split(',');
            recordsIds.add(splitErrorInfo[1].replace(' id=', '').replace('}', ''));
            errors.add(splitErrorInfo[0].replace('{errors=', ''));
        }

        for (Integer i = 0; i < recordsIds.size(); i++)
        {
            if (!errors[i].isWhitespace())
            {
                errorLog += recordsIds[i] + ' ';
                errorLog += '(' + errors[i] + ')\n';
            }
        }

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new List<String> { emailInfo.Email__c });
        mail.setSenderDisplayName(emailInfo.SenderDisplayName__c);
        mail.setSubject(emailInfo.Subject__c);
        mail.setPlainTextBody(errorLog);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

    public static Messaging.SingleEmailMessage generateEmailMessage(Id recordId, Id templateId, Email_Info__mdt mailingInfo)
    {
        if (recordId == null || templateId == null || mailingInfo == null)
        {
            return null;
        }

        Messaging.SingleEmailMessage emailMessage = Messaging.renderStoredEmailTemplate(templateId, UserInfo.getUserId(), recordId);
        emailMessage.setToAddresses(mailingInfo.Recipients__c.trim().split(';'));
        emailMessage.setSenderDisplayName(mailingInfo.SenderDisplayName__c);
        emailMessage.setSubject(mailingInfo.Subject__c);
        emailMessage.saveAsActivity = false;
        return emailMessage;
    }

    public static void sendEmail(List<Messaging.SingleEmailMessage> emailMessages)
    {
        if (emailMessages.isEmpty() && Test.isRunningTest())
        {
            return;
        }
        Messaging.sendEmail(emailMessages);
    }
}