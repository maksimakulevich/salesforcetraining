public class AddPrimaryContact implements Queueable
{
    private Contact contact;
    private String state;

    public AddPrimaryContact(Contact contact, String state)
    {
        this.contact = contact;
        this.state = state;
    }

    public void execute(QueueableContext qCtx)
    {
        List<Account> listOfAccounts = [SELECT Id, Name, (SELECT Id, FirstName, LastName FROM Contacts)
        FROM Account WHERE BillingState = :state LIMIT 200];
        List<Contact> listOfContacts = new List<Contact>();

        for (Account account : listOfAccounts)
        {
            Contact contact = this.contact.clone(false, false, false, false);
            contact.AccountId = account.Id;
            listOfContacts.add(contact);
        }

        if (listOfContacts.size() > 0)
        {
            insert listOfContacts;
        }
    }
}