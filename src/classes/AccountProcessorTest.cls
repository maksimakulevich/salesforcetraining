@IsTest
public class AccountProcessorTest
{
    @IsTest
    public static void TestAccountProcessor()
    {
        Account acc = new Account(Name = 'Test Account');
        insert acc;

        Contact cont = new Contact(FirstName = 'Tom', LastName = 'Thompson', AccountId = acc.Id);
        insert cont;

        List<Id> listOfAccId = new List<Id>();
        listOfAccId.add(acc.Id);

        Test.startTest();
        AccountProcessor.countContacts(listOfAccId);
        Test.stopTest();

        Account testAcc = [
                SELECT Number_of_Contacts__c
                FROM Account
                WHERE Id = :acc.Id
                LIMIT 1];
        System.assertEquals(1, Integer.valueOf(testAcc.Number_of_Contacts__c));
    }
}