@IsTest
global with sharing class RateUpdateWsMock implements HttpCalloutMock
{

	global Integer statusCode;
	global String responseBody;

	global RateUpdateWsMock(Integer statusCode, String responseBody)
	{
		this.statusCode = statusCode;
		this.responseBody = responseBody;
	}

	global HttpResponse respond(HttpRequest request)
	{
		HttpResponse response = new HttpResponse();
		response.setHeader('Content-Type', 'application/json');
		response.setBody(responseBody);
		response.setStatusCode(statusCode);
		return response;
	}
}