public with sharing class RateUpdateWs implements Schedulable
{

	@TestVisible private static final String USD_ISO_CODE = 'USD';

	@future (callout = true)
	public static void getExchangeRates()
	{
		List<Open_Exchange_Rates_Settings__mdt> openExchangeRatesEndpointData = Open_Exchange_Rates_Settings__mdt.getAll().values();

		String endpoint = openExchangeRatesEndpointData[0].Endpoint__c;
		endpoint = endpoint.replace('{0}', openExchangeRatesEndpointData[0].App_Id__c);
		endpoint = endpoint.replace('{1}', openExchangeRatesEndpointData[0].Currencies__c);

		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(endpoint);
		request.setMethod('GET');
		HttpResponse response = http.send(request);

		Integer statusCode = response.getStatusCode();
		Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());

		if (statusCode == 200 && !Test.isRunningTest())
		{
			String requestBody = processResponse(results);
			updateDatedConversionRates(requestBody);

		}
		else if (statusCode >= 400)
		{
			//MailSender.sendErrorReport((List<String>) results.get('message'), 'RateUpdateWs');
		}
	}

	@TestVisible
	private static String processResponse(Map<String, Object> requestResults)
	{
		Object unixTimestamp = (Object) requestResults.get('timestamp');
		Datetime timestamp = Datetime.newInstance((Long) unixTimestamp * 1000);

		List<DatedConversionRate> datedConversionRates = [
			SELECT Id, IsoCode, ConversionRate
			FROM DatedConversionRate
			WHERE StartDate = :timestamp.date()
				AND IsoCode != :USD_ISO_CODE];

		Map<String, DatedConversionRate> datedConversionRatesByCurrencyIsoCode = new Map<String, DatedConversionRate>();
		for (DatedConversionRate datedConversionRate : datedConversionRates)
		{
			datedConversionRatesByCurrencyIsoCode.put(datedConversionRate.IsoCode, datedConversionRate);
		}

		List<String> subRequests = new List<String>();
		Map<String, Object> conversionRates = (Map<String, Object>) requestResults.get('rates');

		if (!datedConversionRates.isEmpty())
		{
			for (String isoCode : conversionRates.keySet())
			{
				if (datedConversionRatesByCurrencyIsoCode.containsKey(isoCode))
				{
					subRequests.add(createSubRequest(datedConversionRatesByCurrencyIsoCode.get(isoCode),
						conversionRates.get(isoCode), timestamp));
				}
				else
				{
					subRequests.add(createSubRequest(new DatedConversionRate(IsoCode = isoCode),
						conversionRates.get(isoCode), timestamp));
				}
			}
		}
		else
		{
			for (String isoCode : conversionRates.keySet())
			{
				subRequests.add(createSubRequest(new DatedConversionRate(IsoCode = isoCode),
					conversionRates.get(isoCode), timestamp));
			}
		}

		String requestBody = '{ \"batchRequests\" : [';
		requestBody += String.join(subRequests, ',');
		requestBody += ']}';

		return requestBody;
	}

	@TestVisible
	private static String createSubRequest(DatedConversionRate datedConversionRate, Object conversionRate, Datetime timestamp)
	{
		String method = 'POST';
		String url = 'v53.0/sobjects/DatedConversionRate';
		if (datedConversionRate.Id != null)
		{
			method = 'PATCH';
			url += '/' + datedConversionRate.Id;

		}
		return '{ \"method\" : \"' + method + '\", \"url\" : \"' + url + '\", \"richInput\" : '
			+ '{ \"IsoCode\" : \"' + datedConversionRate.IsoCode + '\", \"ConversionRate\" : '
			+ conversionRate + ', \"StartDate\" : \"' + timestamp.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') + '\" }}';
	}

	@TestVisible
	private static void updateDatedConversionRates(String requestBody)
	{
		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());
		request.setHeader('Content-Type', 'application/json');
		request.setEndpoint(Url.getSalesforceBaseUrl().toExternalForm() + '/services/data/v53.0/composite/batch');
		request.setBody(requestBody);
		request.setMethod('POST');

		HttpResponse response = http.send(request);
		Integer statusCode = response.getStatusCode();
		Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());

		if (statusCode == 200)
		{
			if ((Boolean) results.get('hasErrors'))
			{
				Map<String, Object> records = (Map<String, Object>) results.get('results');
				List<Object> recordsWithErrors = (List<Object>) records.get('result');
				MailSender.sendErrorReport(recordsWithErrors, 'RateUpdateWs');
			}
		}
		else if (statusCode >= 400)
		{
			List<Object> errorsResults = (List<Object>) results.get('results');
			MailSender.sendErrorReport(errorsResults, '>400', 'RateUpdateWs');
		}
	}

	public void execute(SchedulableContext schedulableContext)
	{
		getExchangeRates();
	}
}