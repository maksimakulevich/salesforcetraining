public with sharing class LeadTriggerHandler implements TriggerTemplate.Handler
{

    private List<Lead> newValues;
    private Map<Id, Lead> oldValues;

    public void setValues(List<SObject> newValues, Map<Id, SObject> oldValues)
    {
        this.newValues = newValues;
        this.oldValues = (Map<Id, Lead>) oldValues;
    }

    public void handle(TriggerTemplate.TriggerAction action)
    {
        if ((action == TriggerTemplate.TriggerAction.beforeInsert) || (action == TriggerTemplate.TriggerAction.beforeUpdate))
        {
            LeadService.updateLeadStatus(this.newValues, this.oldValues);
        }
        if (action == TriggerTemplate.TriggerAction.beforeUpdate)
        {
            LeadService.preventLeadConversionIfPartnerAgreementFieldIsBlank(this.newValues, this.oldValues);
        }
        if (action == TriggerTemplate.TriggerAction.afterInsert)
        {
            LeadService.createPartnerAgreementRelatedToLead(this.newValues, this.oldValues);
        }
        if ((action == TriggerTemplate.TriggerAction.afterUpdate) || (action == TriggerTemplate.TriggerAction.afterDelete))
        {
            LeadService.deletePartnerAgreementPreviouslyRelatedToLead(this.oldValues);
        }
    }
}