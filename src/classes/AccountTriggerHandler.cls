public with sharing class AccountTriggerHandler implements TriggerTemplate.Handler
{

    private List<Account> newValues;
    private Map<Id, Account> oldValues;

    public void setValues(List<SObject> newValues, Map<Id, SObject> oldValues)
    {
        this.newValues = newValues;
        this.oldValues = (Map<Id, Account>) oldValues;
    }

    public void handle(TriggerTemplate.TriggerAction action)
    {
        if ((action == TriggerTemplate.TriggerAction.beforeInsert) || (action == TriggerTemplate.TriggerAction.beforeUpdate))
        {
            AccountService.matchAccountBillingAddress(this.newValues);
        }
    }
}