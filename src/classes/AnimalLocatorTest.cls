@IsTest
public with sharing class AnimalLocatorTest
{

    @IsTest
    static void animalLocatorTest()
    {
        Test.setMock(HttpCalloutMock.class, new AnimalLocatorMock());
        String result = AnimalLocator.getAnimalNameById(1);
        String expectedResult = 'chicken';
        System.assertEquals(expectedResult, result);
    }
}