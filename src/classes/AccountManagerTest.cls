@IsTest
public with sharing class AccountManagerTest
{

    @IsTest
    static void getAccountTest()
    {
        Id accountId = createTestAccountRecord();

        List<Contact> contacts = new List<Contact> { new Contact(LastName = 'Holland', AccountId = accountId), new Contact(LastName = 'Garfield', AccountId = accountId) };

        insert contacts;

        RestRequest request = new RestRequest();
        request.requestURI = 'https://XXXX.salesforce.com/services/apexrest/Accounts/' + accountId + '/contacts';
        request.httpMethod = 'GET';
        RestContext.request = request;

        Account actualAccount = AccountManager.getAccount();

        System.assert(actualAccount != null);
        System.assertEquals(2, actualAccount.Contacts.size());
    }

    static Id createTestAccountRecord()
    {
        Account account = new Account(Name = 'Test Account');
        insert account;
        return account.Id;
    }
}