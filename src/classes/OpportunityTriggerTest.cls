@IsTest
private class OpportunityTriggerTest
{

	private static List<Opportunity> opportunities;
	private static Date expectedClosedDateUponCreation;
	private static Date expectedClosedDateUponClosure;

	static void dataSetup()
	{
		opportunities = new List<Opportunity>();
		for (Integer i = 0; i < 4; i++)
		{
			opportunities.add(new Opportunity(Name = 'TestData'));
		}

		expectedClosedDateUponCreation = Date.today().addMonths(3);
		expectedClosedDateUponClosure = Date.today();
	}

	@IsTest
	static void fillInCloseDateIfBlankTest()
	{
		dataSetup();

		for (Integer i = 0; i < 4; i++)
		{
			System.assert(opportunities[i].CloseDate == null);
		}

		insert opportunities;

		List<Opportunity> updatedOpportunities = [
			SELECT CloseDate
			FROM Opportunity
			WHERE Id IN :opportunities];

		for (Integer i = 0; i < 4; i++)
		{
			System.assertEquals(expectedClosedDateUponCreation, updatedOpportunities[i].CloseDate);
		}
	}

	@IsTest
	static void fillInCloseDateWhenClosedTest()
	{
		dataSetup();

		insert opportunities;

		opportunities[0].StageName = Label.Opportunity_Stage_Closed_Lost;
		opportunities[1].StageName = Label.Opportunity_Stage_Closed_Lost;
		opportunities[2].StageName = Label.Opportunity_Stage_Closed_Won;
		opportunities[3].StageName = Label.Opportunity_Stage_Closed_Won;

		Test.startTest();
			update opportunities;
		Test.stopTest();

		List<Opportunity> updatedOpportunities = [
			SELECT CloseDate
			FROM Opportunity
			WHERE Id IN :opportunities];

		for (Integer i = 0; i < 4; i++)
		{
			System.assertEquals(expectedClosedDateUponClosure, updatedOpportunities[i].CloseDate);
		}
	}

	@IsTest
	static void blockUpdatingIfOpportunityIsClosedAsLostTest()
	{
		dataSetup();

		insert opportunities;

		opportunities[0].StageName = Label.Opportunity_Stage_Closed_Lost;
		opportunities[1].StageName = Label.Opportunity_Stage_Closed_Won;

		Test.startTest();
			update opportunities;
		Test.stopTest();

		opportunities[0].Name = 'TestChanged';
		opportunities[1].CloseDate = Date.newInstance(2022, 2, 28);

		String actualDmlException;

		try
		{
			update opportunities;
		}
		catch (DmlException dmlException)
		{
			actualDmlException = dmlException.getMessage();
		}

		System.assert(actualDmlException.contains(Label.Cannot_Update_Closed_Lost_Opportunities));
	}

	@IsTest
	static void blockUpdatingIfOpportunityIsClosedAsWonTest()
	{
		dataSetup();

		insert opportunities;

		opportunities[0].StageName = Label.Opportunity_Stage_Closed_Won;
		opportunities[1].StageName = Label.Opportunity_Stage_Closed_Lost;

		Test.startTest();
		update opportunities;
		Test.stopTest();

		opportunities[0].Name = 'TestChanged';
		opportunities[1].CloseDate = Date.newInstance(2022, 2, 28);

		String actualDmlException;

		try
		{
			update opportunities;
		}
		catch (DmlException dmlException)
		{
			actualDmlException = dmlException.getMessage();
		}

		System.assert(actualDmlException.contains(Label.Cannot_Update_Closed_Won_Opportunities));
	}

	@IsTest
	static void clearFieldsRelatedToClosedLostStatusTest()
	{
		dataSetup();

		insert opportunities;

		opportunities[0].StageName = Label.Opportunity_Stage_Closed_Lost;
		opportunities[0].Closed_Lost_Reason__c = 'Lack of candidates';
		opportunities[0].Closed_Lost_Comments__c = 'Test comment';
		opportunities[1].StageName = Label.Opportunity_Stage_Closed_Lost;
		opportunities[1].Closed_Lost_Reason__c = 'Rates too high';
		opportunities[1].Closed_Lost_Comments__c = 'Test comment';
		opportunities[2].StageName = Label.Opportunity_Stage_Closed_Won;
		opportunities[3].StageName = Label.Opportunity_Stage_Closed_Won;

		update opportunities;

		opportunities[0].StageName = Label.Opportunity_Stage_Prospecting;
		opportunities[1].StageName = Label.Opportunity_Stage_Prospecting;

		update opportunities[0];
		update opportunities[1];

		List<Opportunity> updatedOpportunities = [
			SELECT Closed_Lost_Comments__c, Closed_Lost_Reason__c
			FROM Opportunity
			WHERE Id IN :opportunities];

		for (Integer i = 0; i < 2; i++)
		{
			System.assert(String.isEmpty(updatedOpportunities[i].Closed_Lost_Reason__c));
			System.assert(String.isEmpty(updatedOpportunities[i].Closed_Lost_Comments__c));
		}
	}

	@IsTest
	static void setStageNameUponCreationTest()
	{
		dataSetup();

		insert opportunities;

		List<Opportunity> updatedOpportunities = [
			SELECT StageName
			FROM Opportunity
			WHERE Id IN :opportunities];

		for (Integer i = 0; i < 4; i++)
		{
			System.assertEquals(Label.Opportunity_Stage_Prospecting, updatedOpportunities[i].StageName);
		}
	}

	@IsTest
	static void sendEmailUponOpportunityClosureTest()
	{
		dataSetup();

		insert opportunities;

		opportunities[0].StageName = Label.Opportunity_Stage_Closed_Lost;
		opportunities[1].StageName = Label.Opportunity_Stage_Closed_Lost;
		opportunities[2].StageName = Label.Opportunity_Stage_Closed_Won;
		opportunities[3].StageName = Label.Opportunity_Stage_Closed_Won;

		update opportunities;

		System.assert(Limits.getEmailInvocations() == 1);
	}
}