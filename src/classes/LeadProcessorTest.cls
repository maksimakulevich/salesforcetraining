@IsTest
public class LeadProcessorTest
{
    @IsTest
    public static void TestLeadProcessor()
    {
        List<Lead> leads = new List<Lead>();
        for (Integer i = 0; i < 200; i++)
        {
            leads.add(new Lead(FirstName = 'FirstName', LastName = 'LastName', Company = 'Test ' + i));
        }
        insert leads;

        Test.startTest();
        LeadProcessor leadProcessor = new LeadProcessor();
        Database.executeBatch(leadProcessor);
        Test.stopTest();
    }
}